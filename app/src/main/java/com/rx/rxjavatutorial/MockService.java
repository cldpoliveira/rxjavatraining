package com.rx.rxjavatutorial;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by claudio.oliveira on 18/09/17.
 */

public class MockService {

    public List<String> getNames() {
        try {
            // "Simulate" the delay of network.
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return createNames();
    }


    private List<String> createNames() {
        List<String> names = new ArrayList<>();
        names.add("Adriana");
        names.add("José");
        names.add("Maria");
        names.add("João");
        return names;
    }
}
