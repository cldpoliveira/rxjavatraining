package com.rx.rxjavatutorial;

import com.rx.rxjavatutorial.model.User;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by claudio.oliveira on 18/09/17.
 */

public interface RxJavaApi {
    @GET("users/cldoliveira")
    Observable<User> getUser();
}
