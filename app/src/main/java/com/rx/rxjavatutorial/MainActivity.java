package com.rx.rxjavatutorial;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.gson.GsonBuilder;
import com.rx.rxjavatutorial.model.User;

import java.util.List;
import java.util.concurrent.Callable;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Observer;
import rx.Single;
import rx.SingleSubscriber;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    MockService mock = new MockService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onJustClick(View v) {
        Observable<String> observable = Observable.just("Hello world");

        observable.subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                Log.d("RxJava - ", s);
            }
        });
    }

    public void onObservableCreate(View v) {
        Observable<List<String>> observable = Observable.create(new Observable.OnSubscribe<List<String>>() {
            @Override
            public void call(Subscriber<? super List<String>> subscriber) {
                List<String> names = mock.getNames();
                subscriber.onNext(names);
                subscriber.onCompleted();
            }
        });

        observable.doOnSubscribe(new Action0() {
            @Override
            public void call() {
                Log.d("RxJava - ", "Requisicao iniciada");
            }
        }).subscribeOn(Schedulers.newThread()) // Onde o processamento ocorre
                .observeOn(AndroidSchedulers.mainThread()) // onde o subscribe é executado
                .subscribe(new Observer<List<String>>() {
            @Override
            public void onCompleted() {
                Log.d("RxJava - ", "Requisicao completou...");
            }

            @Override
            public void onError(Throwable e) {
                Log.d("RxJava - ", "Erro na requisição");
            }

            @Override
            public void onNext(List<String> strings) {
                Log.d("RxJava - ", "Nomes recebidos..");
                for(String s: strings) {
                    Log.d("RxJava - ", s);
                }
            }
        });
    }

    public void onSingleClick(View view) {
        Single<List<String>> single = Single.fromCallable(new Callable<List<String>>() {
            @Override
            public List<String> call() throws Exception {
                return mock.getNames();
            }
        });

        single.doOnSubscribe(new Action0() {
            @Override
            public void call() {
                Log.d("RxJava - ", "Requisicao iniciou...");
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleSubscriber<List<String>>() {
                    @Override
                    public void onSuccess(List<String> strings) {
                        Log.d("RxJava - ", "Nomes recebidos..");
                        for (String s: strings) {
                            Log.d("RxJava - ", s);
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        Log.d("RxJava - ", "Erro na recuperacao da lista");
                    }
                });
    }

    public void onRetrofitJoined(View view) {
        RxJavaApi api = provideRxApi(getRetrofit());

        api.getUser()
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        Log.d("RxJava - ", "metodo chamado");
                    }
                })
                .doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.d("RxJava - ", "ocorreu um erro");
                        Log.d("RxJava - ", throwable.getMessage());
                    }
                })
                .subscribe(new Action1<User>() {
                    @Override
                    public void call(User user) {
                        Log.d("RxJava - ", "User retornado...");
                        Log.d("RxJava - ", user.getLogin());
                        Log.d("RxJava - ", user.getType());

                    }
                });
    }

    private Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();
    }

    private RxJavaApi provideRxApi(Retrofit retrofit) {
        return retrofit.create(RxJavaApi.class);
    }
}
