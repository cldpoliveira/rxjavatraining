package com.rx.rxjavatutorial.model;

/**
 * Created by claudio.oliveira on 18/09/17.
 */

public class User {
    private String login;
    private String type;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
